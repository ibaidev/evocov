# -*- coding: utf-8 -*-
#
#    Copyright 2020 Ibai Roman
#
#    This file is part of EvoCov.
#
#    EvoCov is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    EvoCov is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with EvoCov. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

import evocov
import ksvmlib


def svm_classification_example(data):
    """
    Classification example with SVMs

    :param data:
    :type data:
    :return:
    :rtype:
    """

    validation = ksvmlib.dm.RandFold(fold_len=0.2, n_folds=1)
    train_set, test_set = validation.get_folds(data)[0]
    transformation = ksvmlib.dm.DataTransformation(train_set)
    train_set = transformation.transform(train_set)
    test_set = transformation.transform(test_set)

    accuracy = ksvmlib.me.Accuracy()

    fitting_method = evocov.fit.Random(
        obj_fun=accuracy.fold_measure,
        max_fun_call=5000,
        nested_fit_method=evocov.fit.Greedy(
            obj_fun=accuracy.fold_measure,
            max_fun_call=1000,
            nested_fit_method=ksvmlib.fit.GridSearch(
                obj_fun=accuracy.fold_measure,
                max_fun_call=300
            )
        )
    )
    svm = ksvmlib.KSVM(fitting_method.get_random_kernel())

    train_validation = ksvmlib.dm.RandFold(fold_len=0.2, n_folds=3)
    log = fitting_method.fit(svm, train_validation.get_folds(
        train_set
    ))
    print("Fitting log: {}".format(log))

    print("Accuracy: {}".format(accuracy.measure(svm, train_set, test_set)))

    ksvmlib.plot.kernel_sort_data(svm, test_set)


def gen_data(n=200):
    """

    :param n:
    :type n:
    :return:
    :rtype:
    """
    data = {}
    n_class = int(n / 2)

    data['X'] = np.vstack((
        np.random.multivariate_normal([1, 1], [[1, 0], [0, 1]], n_class),
        np.random.multivariate_normal([3, 3], [[1, 0], [0, 1]], n_class)
    ))
    data['Y'] = np.vstack((
        np.ones((n_class, 1)),
        np.zeros((n_class, 1)),
    ))

    data['X'] *= np.power(10, np.random.uniform(-5, 5))

    return data


if __name__ == '__main__':
    svm_classification_example(gen_data())
