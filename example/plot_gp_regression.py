# -*- coding: utf-8 -*-
#
#    Copyright 2020 Ibai Roman
#
#    This file is part of EvoCov.
#
#    EvoCov is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    EvoCov is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with EvoCov. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

import evocov
import gplib


def gp_regression_example(data):
    """
    Regression example with GPs

    :param data:
    :type data:
    :return:
    :rtype:
    """

    validation = gplib.dm.Unfold(fold_len=0.2)
    train_set, test_set = validation.get_folds(data)[0]
    transformation = gplib.dm.DataTransformation(train_set)
    train_set = transformation.transform(train_set)
    test_set = transformation.transform(test_set)

    lml = gplib.me.LML()
    bic = gplib.me.BIC()
    meta_metric = evocov.me.MetaMetric([bic, lml])

    fitting_method = evocov.fit.MOECov(
        obj_fun=meta_metric.fold_measure,
        max_fun_call=25000,
        objectives=2,
        nested_fit_method=gplib.fit.MultiStart(
            obj_fun=lml.fold_measure,
            max_fun_call=250,
            nested_fit_method=gplib.fit.LocalSearch(
                obj_fun=lml.fold_measure,
                method="Powell",
                max_fun_call=100
            )
        )
    )

    gp = gplib.GP(
        mean_function=gplib.mea.Fixed(),
        covariance_function=fitting_method.get_random_kernel()
    )

    validation = gplib.dm.Full()

    # We can chain posterior GPs
    log = fitting_method.fit(gp, validation.get_folds(
        train_set
    ))
    print("{}".format(log))
    posterior_gp1 = gp.get_posterior(train_set)

    posterior_lml = posterior_gp1.get_log_likelihood(
        test_set
    )
    print("{}".format(posterior_lml))

    gplib.plot.gp_1d(
        posterior_gp1,
        train_set,
        test_set
    )


def gen_data(n=200):
    """

    :param n:
    :type n:
    :return:
    :rtype:
    """

    gp = gplib.GP(
        mean_function=gplib.mea.Fixed(),
        covariance_function=gplib.ker.Sum([
            gplib.ker.SquaredExponential(),
            gplib.ker.WhiteNoise(ov2=1e-2)
        ])
    )

    data = dict()
    data['X'] = np.arange(10., 20., 10./n)[:, None]
    data['Y'] = gp.sample(data['X'], n_samples=1)

    data['X'] *= np.power(10, np.random.uniform(-5, 5))
    data['Y'] *= np.power(10, np.random.uniform(-5, 5))

    return data


if __name__ == '__main__':
    gp_regression_example(gen_data())
