from .random import Random
from .greedy import Greedy
from .evocov import EvoCov
from .moecov import MOECov
from .genetic_programming import GeneticProgramming
from .go_with_the_first import GoWithTheFirst