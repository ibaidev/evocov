from . import fitting_methods as fit
from . import metrics as me

__version__ = '0.1.0'