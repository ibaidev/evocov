evocov.kernel\_functions package
================================

Submodules
----------

evocov.kernel\_functions.gen\_prog\_kernel module
-------------------------------------------------

.. automodule:: evocov.kernel_functions.gen_prog_kernel
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: evocov.kernel_functions
   :members:
   :undoc-members:
   :show-inheritance:
