evocov package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   evocov.fitting_methods
   evocov.gen_prog_tools
   evocov.kernel_functions
   evocov.metrics

Module contents
---------------

.. automodule:: evocov
   :members:
   :undoc-members:
   :show-inheritance:
