evocov.fitting\_methods package
===============================

Submodules
----------

evocov.fitting\_methods.evocov module
-------------------------------------

.. automodule:: evocov.fitting_methods.evocov
   :members:
   :undoc-members:
   :show-inheritance:

evocov.fitting\_methods.genetic\_programming module
---------------------------------------------------

.. automodule:: evocov.fitting_methods.genetic_programming
   :members:
   :undoc-members:
   :show-inheritance:

evocov.fitting\_methods.go\_with\_the\_first module
---------------------------------------------------

.. automodule:: evocov.fitting_methods.go_with_the_first
   :members:
   :undoc-members:
   :show-inheritance:

evocov.fitting\_methods.greedy module
-------------------------------------

.. automodule:: evocov.fitting_methods.greedy
   :members:
   :undoc-members:
   :show-inheritance:

evocov.fitting\_methods.kernel\_fitting\_method module
------------------------------------------------------

.. automodule:: evocov.fitting_methods.kernel_fitting_method
   :members:
   :undoc-members:
   :show-inheritance:

evocov.fitting\_methods.moecov module
-------------------------------------

.. automodule:: evocov.fitting_methods.moecov
   :members:
   :undoc-members:
   :show-inheritance:

evocov.fitting\_methods.random module
-------------------------------------

.. automodule:: evocov.fitting_methods.random
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: evocov.fitting_methods
   :members:
   :undoc-members:
   :show-inheritance:
