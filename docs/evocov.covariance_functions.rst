evocov.covariance\_functions package
====================================

Submodules
----------

evocov.covariance\_functions.gen\_prog\_cov module
--------------------------------------------------

.. automodule:: evocov.covariance_functions.gen_prog_cov
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: evocov.covariance_functions
   :members:
   :undoc-members:
   :show-inheritance:
